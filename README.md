[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

# :warning: **This project is not maintained!**

# General information
This ROS package describes:
- Sensors
- End-effectors
- Work-cells
- Robots

<img src="/fanuc_r1000ia_grinding/fanuc_r1000ia_sls_2_grinding_support/r1000ia_grinding.png" align="center" height="200">
<img src="/fanuc_r1000ia_grinding/fanuc_r1000ia_ensenso_n10_grinding_support/r1000ia_grinding.png" align="center" height="200">
<img src="/fanuc_m10ia_kinect/fanuc_m10ia_kinect_support/m10ia_kinect.png" align="center" height="200">
<img src="/fanuc_m10ia_ensenso/fanuc_m10ia_ensenso_support/m10ia_ensenso.png" align="center" height="200">
<img src="/ur10_ensenso/ur10_ensenso_support/ur10_ensenso.png" align="center" height="200">
<img src="/fanuc_m900ib_vision/fanuc_m900ib_sls_2_vision_support/m900ib700.png" align="center" height="200">

# Directories in the project

| Directory  | Description
------------ | -----------
`fanuc_m10ia_ensenso` | Fanuc M10iA with a Ensenso mounted on the tool
`fanuc_m10ia_kinect` | Fanuc M10iA with a Kinect mounted on the tool
`fanuc_m900ib_vision` | Fanuc M900iB/700 with a SLS-2 mounted on the FSW head
`fanuc_r1000ia_grinding` | Fanuc R1000iA80f + Grinding end-effector + 3D sensor ( Ensenso or SLS-2) + grinding work-cell
`ur10_ensenso` | Univeral Robot UR10 + Ensenso N10 + Excelcar work-cell

| `sensors`: Directory  | Description
--------------------- | -----------
`ensenso_n10_description` | Ensenso N10
`kinect_v1_description` | Microsoft Kinect for Xbox 360 without the support
`sls_2_description` | David SLS-2

# Dependencies
- [Robot Operating System](http://wiki.ros.org/ROS/Installation)
- [`industrial-core`](http://wiki.ros.org/industrial_core)
- [`fanuc`](http://wiki.ros.org/fanuc)
- [`fanuc_experimental`](http://wiki.ros.org/fanuc_experimental)
- [`universal_robot`](https://github.com/ros-industrial/universal_robot) :warning: Use source code!
- [`industrial_calibration`](https://github.com/ros-industrial/industrial_calibration)
- [`yaml-cpp`](https://launchpad.net/ubuntu/+source/yaml-cpp)
- [PCL](https://github.com/PointCloudLibrary/pcl) 1.8.0 with [davidSDK](https://gitlab.com/InstitutMaupertuis/davidSDK) support

This package has been tested with Ubuntu 16.04 and ROS Kinetic.

# Testing the robots
`roslaunch fanuc_r1000ia_ensenso_n10_grinding_support test_r1000ia_grinding.launch`

`roslaunch fanuc_r1000ia_sls_2_grinding_support test_r1000ia_grinding.launch`
