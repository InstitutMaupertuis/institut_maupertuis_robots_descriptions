 [![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr) Institut Maupertuis robots descriptions
===

SLS-2 configuration files
---
These files are produced by the SLS-2 when running the calibration with the David software.
They are not parsed / used by the package itself, they are just here for information.

The files `sls_2_calibration.yaml`and `sls_2_camera_def.yaml` contains values copied from these configuration files.

