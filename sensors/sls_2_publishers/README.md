 [![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr) Institut Maupertuis robots descriptions
===

This package provides ROS nodes that publishes images and or point clouds from a David SLS-2 sensor on ROS topics.
Please review [the PCL tutorial](http://pointclouds.org/documentation/tutorials/davidsdk.php) and `sls_2_images.launch` in order to understand how to launch/use this node.

